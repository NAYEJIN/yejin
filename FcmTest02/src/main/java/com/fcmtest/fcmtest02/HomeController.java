package com.fcmtest.fcmtest02;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeController {
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home() {
		
		return "home";
	}
	
	@RequestMapping(value = "send", method = RequestMethod.POST)
	public String send(FcmDto dto) {
		FcmUtil util = new FcmUtil();
		util.send_FCM(dto);
		
		return "redirect:/";
	}
}
