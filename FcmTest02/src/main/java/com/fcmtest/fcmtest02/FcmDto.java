package com.fcmtest.fcmtest02;

public class FcmDto {
	String tokenId = "cj93O-ukTjqKA510hUgi2X:APA91bE5USF5oh7SU0v4lqZFade2Rp3rLpMlqvk8UmYhKZ44BaruG63kQFvfkFNBNprDQ44cutUf2JcqTgUCqJrigCA7smx6xJ36GpX1NXoEfFvtXH95y1-_yc2NmTKBIKmVLV3V5SRN";
	String send_title;
	String send_content;
	
	public String getTokenId() {
		return tokenId;
	}
	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}
	public String getSend_title() {
		return send_title;
	}
	public void setSend_title(String send_title) {
		this.send_title = send_title;
	}
	public String getSend_content() {
		return send_content;
	}
	public void setSend_content(String send_content) {
		this.send_content = send_content;
	}	
}
