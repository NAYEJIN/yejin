package com.fcmtest.fcmtest02;

import java.io.FileInputStream;

import org.springframework.stereotype.Component;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.AndroidConfig;
import com.google.firebase.messaging.AndroidNotification;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.Message;

@Component
public class FcmUtil {
	public void send_FCM(FcmDto dto) {
		try {
			//본인의 json 파일 경로 입력
			FileInputStream refreshToken = new FileInputStream("C:\\Users\\everitime\\eclipse-workspace\\FcmTest02\\src\\main\\webapp\\resources\\fcmtest01-50f2f-6604d2913670.json");
			
			FirebaseOptions options = new FirebaseOptions.Builder()
					.setCredentials(GoogleCredentials.fromStream(refreshToken))
					.setDatabaseUrl("https://fcm.googleapis.com/fcm/send")
					.build();
			
			//Firebase 처음 호출시에만 initailizing 처리
			if(FirebaseApp.getApps().isEmpty()) {
				FirebaseApp.initializeApp(options);
			}
			
			//String registrationToken = 안드로이드 토큰 입력
			String registrationToken = dto.getTokenId();
			
			//message 작성
			Message msg = Message.builder()
					.setAndroidConfig(AndroidConfig.builder()
							.setTtl(3600 * 1000) // 1 hour in milliseconds
							.setPriority(AndroidConfig.Priority.NORMAL)
							.setNotification(AndroidNotification.builder()
									.setTitle(dto.getSend_title())
									.setBody(dto.getSend_content())
									.setIcon("stock_ticker_update")
									.setColor("#f45342")
									.build())
							.build())
					.setToken(registrationToken)
					.build();
			
			//메세지를 FirebaseMessaging에 보내기
			String response = FirebaseMessaging.getInstance().send(msg);
			//결과 출력
			System.out.println("Successfully sent message: " + response);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
}
