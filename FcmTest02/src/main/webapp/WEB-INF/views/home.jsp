<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
	<head>
		<title>Home</title>
	</head>
	
	<body>
		<form action="send" method="post">
			<table>
				<tr>
					<td>
						<input type="text" name="send_title" placeholder="title">
					</td>
				</tr>
				<tr>
					<td>
						<textarea name="send_content" placeholder="content"></textarea>
					</td>
				</tr>
				<tr>
					<td>
						<button style="width:100%">보내기</button>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
